﻿Run:
Add-Migration InitialCreate
Update-Database

Comments:

JQuery
- duplicated code - refactor JQuery functions in seperate files
- version conflict - different JQuery reference for .autocomplete. most layout are set to ""

API calls
- autocomplete call and cascading calls should be all in ProductRestController

Country/City
- Cascading of Country/City is buggy for edit view. City should be requested via AJAX and dropdown does not select. Couldnt get Category/SubCategory/MainProduct Example to work.

General
- Identity is missing and therefore authorize filter on the actions
- Email for edited User is missing
- Cleanup and Renaming Project
- Repository Pattern missing




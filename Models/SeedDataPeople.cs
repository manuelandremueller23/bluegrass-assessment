﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MvcMovie.Data;
using System;
using System.Linq;

namespace MvcMovie.Models
{
    public static class SeedDataPeople
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcPeopleContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcPeopleContext>>()))
            {
                if (context.People.Any())
                {
                    return; 
                }

                context.People.AddRange(
                    new People()
                    {
                        Firstname = "Manuel",
                        Surname = "Mueller",
                        City = "Hamburg",
                        Country = "Deutschland",
                        ProfilePic = "picturestring",
                        MobileNumber = "123456789",
                        Gender = "123456789",
                        Email = "test@email.com",

                    },
                    new People()
                    {
                        Firstname = "Tobias",
                        Surname = "Drost",
                        City = "Augsgurg",
                        Country = "Taiwan",
                        ProfilePic = "picturestring",
                        MobileNumber = "987654321",
                        Gender = "123456789",
                        Email = "test@email.com",
                    },
                    new People()
                    {
                        Firstname = "Dieter",
                        Surname = "Hahn",
                        City = "Muenchen",
                        Country = "Suedafrika",
                        ProfilePic = "picturestring",
                        MobileNumber = "111111111",
                        Gender = "123456789",
                        Email = "test@email.com",
                    },
                    new People()
                    {
                        Firstname = "Bert",
                        Surname = "Vogt",
                        City = "Berlin",
                        Country = "Amerika",
                        ProfilePic = "picturestring",
                        MobileNumber = "99999999",
                        Gender = "123456789",
                        Email = "test@email.com",
                    },
                    new People()
                    {
                        Firstname = "Gert",
                        Surname = "Dauch",
                        City = "Leipzig",
                        Country = "Kanada",
                        ProfilePic = "picturestring",
                        MobileNumber = "99999999",
                        Gender = "male",
                        Email = "test@email.com",
                    },
                    new People()
                    {
                        Firstname = "Heiner",
                        Surname = "Lauterbad",
                        City = "Dresden",
                        Country = "Indien",
                        ProfilePic = "picturestring",
                        MobileNumber = "99999999",
                        Gender = "123456789",
                        Email = "test@email.com",
                    }
                ) ; ;
                context.SaveChanges();
            }
        }
    }
}
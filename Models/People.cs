﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcMovie.Models
{
    public class People
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string ProfilePic { get; set; }

        public string MobileNumber { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }
    }
}
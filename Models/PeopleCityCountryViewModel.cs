﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MvcMovie.Models
{
    public class PeopleCityCountryViewModel
    {
        public List<People> People { get; set; }
        public SelectList City { get; set; }
        public SelectList Country { get; set; }
        public string PeopleCity { get; set; }
        public string PeopleCountry { get; set; }
        public string SearchString { get; set; }
    }
}
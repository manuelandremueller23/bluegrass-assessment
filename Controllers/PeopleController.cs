﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMovie.Data;
using MvcMovie.Models;
using Microsoft.AspNetCore.Http;

namespace MvcMovie.Controllers
{
    public class PeopleController : Controller
    {
        private readonly MvcPeopleContext _context;

        public PeopleController(MvcPeopleContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(string peopleCity, string peopleCountry, string searchString)
        {
            IQueryable<string> genreQueryCity = from m in _context.People
                orderby m.City
                select m.City;

            IQueryable<string> genreQueryCountry = from m in _context.People
                orderby m.Country
                select m.Country;

            var people = from m in _context.People
                select m;

            if (!string.IsNullOrEmpty(searchString))
            {
                people = people.Where(s => s.Firstname.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(peopleCity))
            {
                people = people
                    .Where(x => x.City == peopleCity);
            }
            
            if (!string.IsNullOrEmpty(peopleCountry))
            {
                people = people
                    .Where(x => x.Country == peopleCountry);
            }

            var peopleCityCountryVm = new PeopleCityCountryViewModel
            {
                City = new SelectList(await genreQueryCity.Distinct().ToListAsync()),
                Country = new SelectList(await genreQueryCountry.Distinct().ToListAsync()),
                People = await people.ToListAsync()
            };

            return View(peopleCityCountryVm);
        }
        
        public async Task<IActionResult> Client(string peopleCity, string peopleCountry, string searchString)
        {
            IQueryable<string> genreQueryCity = from m in _context.People
                orderby m.City
                select m.City;

            IQueryable<string> genreQueryCountry = from m in _context.People
                orderby m.Country
                select m.Country;

            var people = from m in _context.People
                select m;

            if (!string.IsNullOrEmpty(searchString))
            {
                people = people.Where(s => s.Firstname.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(peopleCity))
            {
                people = people
                    .Where(x => x.City == peopleCity);
            }
            
            if (!string.IsNullOrEmpty(peopleCountry))
            {
                people = people
                    .Where(x => x.Country == peopleCountry);
            }

            var peopleCityCountryVm = new PeopleCityCountryViewModel
            {
                City = new SelectList(await genreQueryCity.Distinct().ToListAsync()),
                Country = new SelectList(await genreQueryCountry.Distinct().ToListAsync()),
                People = await people.ToListAsync()
            };

            return View(peopleCityCountryVm);
        }

        public async Task<IActionResult> Admin(string peopleCity, string peopleCountry, string searchString)
        {
            IQueryable<string> genreQueryCity = from m in _context.People
                orderby m.City
                select m.City;

            IQueryable<string> genreQueryCountry = from m in _context.People
                orderby m.Country
                select m.Country;

            var people = from m in _context.People
                select m;

            if (!string.IsNullOrEmpty(searchString))
            {
                people = people.Where(s => s.Firstname.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(peopleCity))
            {
                people = people
                    .Where(x => x.City == peopleCity);
            }
            
            if (!string.IsNullOrEmpty(peopleCountry))
            {
                people = people
                    .Where(x => x.Country == peopleCountry);
            }

            var peopleCityCountryVm = new PeopleCityCountryViewModel
            {
                City = new SelectList(await genreQueryCity.Distinct().ToListAsync()),
                Country = new SelectList(await genreQueryCountry.Distinct().ToListAsync()),
                People = await people.ToListAsync()
            };

            return View(peopleCityCountryVm);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var people = await _context.People
                .FirstOrDefaultAsync(m => m.Id == id);
            if (people == null)
            {
                return NotFound();
            }

            return View(people);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Firstname,Surname,City,Country,ProfilePic,MobileNumber,Gender,Email")]
            People people)
        {
            if (ModelState.IsValid)
            {
                _context.Add(people);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Admin));
            }

            return View(people);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var people = await _context.People.FindAsync(id);
            if (people == null)
            {
                return NotFound();
            }
            
            List<People> categorylist = new List<People>();

            categorylist = (from category in _context.People
                select category).ToList();

            categorylist.Insert(0, new People { Id = 0, Firstname = "Select" });

            ViewBag.ListofCategory = categorylist;

            return View(people);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,Firstname,Surname,Country,City,ProfilePic,MobileNumber,Gender,Email")]
            People people, People objcategory, IFormCollection formCollection)
        {

            List<People> categorylist = new List<People>();
            categorylist = (from category in _context.People
                select category).ToList();
            categorylist.Insert(0, new People { Id = 0, Country = "Select" });
            
            ViewBag.ListofCategory = categorylist;
            
            if (id != people.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(people);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.People.Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Admin));
            }

            return View(people);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var people = await _context.People
                .FirstOrDefaultAsync(m => m.Id == id);
            if (people == null)
            {
                return NotFound();
            }

            return View(people);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var people = await _context.People.FindAsync(id);
            _context.People.Remove(people);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Admin));
        }

//        public JsonResult GetSubCategory(int CategoryID)
//        {
//            List<SubCategory> subCategorylist = new List<SubCategory>();
//
//            // ------- Getting Data from Database Using EntityFrameworkCore -------
//            subCategorylist = (from subcategory in _context.SubCategory
//                where subcategory.CategoryID == CategoryID
//                select subcategory).ToList();
//
//            // ------- Inserting Select Item in List -------
//            subCategorylist.Insert(0, new SubCategory { SubCategoryID = 0, SubCategoryName = "Select" });
//
//
//            return Json(new SelectList(subCategorylist, "SubCategoryID", "SubCategoryName"));
//        }
//
//        public JsonResult GetProducts(int SubCategoryID)
//        {
//            List<MainProduct> productList = new List<MainProduct>();
//
//            // ------- Getting Data from Database Using EntityFrameworkCore -------
//            productList = (from product in _context.MainProduct
//                where product.SubCategoryID == SubCategoryID
//                select product).ToList();
//
//            // ------- Inserting Select Item in List -------
//            productList.Insert(0, new MainProduct { ProductID = 0, ProductName = "Select" });
//
//            return Json(new SelectList(productList, "ProductID", "ProductName"));
//        }
    }
}
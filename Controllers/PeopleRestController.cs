﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcMovie.Data;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    [Route("api/people")]
    public class PeopleRestController : Controller
    {
        private readonly MvcPeopleContext _context;
        
        public PeopleRestController(MvcPeopleContext context)
        {
            _context = context;
        }
        
        [Produces("application/json")]
        [HttpGet("search")]
        public async Task<IActionResult> Search()
        {
            try
            {
                string term = HttpContext.Request.Query["term"].ToString();
                var people = _context.People.Where(p => p.Firstname.Contains(term)).Select(p => p.Firstname).ToList();
                
                return Ok(people);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using MvcMovie.Models;

namespace MvcMovie.Data
{
    public class MvcPeopleContext : DbContext
    {
        public MvcPeopleContext (DbContextOptions<MvcPeopleContext> options)
            : base(options)
        {
        }

        public DbSet<People> People { get; set; }
    }
}